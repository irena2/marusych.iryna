import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by irena on 4/18/2016.
 */
public class Lab3_3 {
    public static void main(String[] args) throws IOException {
//        Вывести все простые числа от 1 до N. N задается пользователем через консоль

        System.out.print("Enter a number: ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String num = br.readLine();
        int n = Integer.parseInt(num);
        int dividers;

        for (int i = 2; i <= n; i++) {
            dividers = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0)
                    dividers++;
            }
            if (dividers <= 2)
                System.out.print(i+ " " );

        }
    }
}
