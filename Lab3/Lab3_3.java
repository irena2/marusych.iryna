import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Viper on 16.04.2016.
 */
public class Lab3_3 {
    public static void main(String[] args) throws IOException {
//        ������� ��� ������� ����� �� 1 �� N. N �������� ������������� ����� �������

        System.out.print("Enter a number: ");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String num = br.readLine();
        int n = Integer.parseInt(num);

        for (int i = 1; i <= n; i++) {
            if (i == 2 || i == 3 || i == 5 || i == 7) {
                System.out.println(i);
            } else if (i % 2 != 0 & i % 3 != 0 & i % 5 != 0 & i % 7 != 0) {
                System.out.println(i);
            }
        }
    }
}
