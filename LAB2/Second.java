/**
 * Created by Viper on 10.04.2016.
 */
public class Second {
    public static void main (String [] args){
        byte b = 2;
        String st = Byte.toString(b);
        System.out.println("st = " + st);

        b = Byte.parseByte(st);
        System.out.println(b+2);

        short s = 12;
        String st1 = Short.toString(s);
        System.out.println("st1 = " + st1 + 879);

        s = Short.parseShort(st1);
        System.out.println(s-1);

        int i = 14;
        String str = Integer.toString(i);
        System.out.println("str = " + str);

        i = Integer.parseInt(str);
        System.out.println(i + 2);

        float f = 14.87f;
        String str1 = Float.toString(f);
        System.out.println("str1 = " + str1);

        f = Float.parseFloat(str1);
        System.out.println(f);

        double d = 14890.576;
        String str2 = Double.toString(d);
        System.out.println("str2 = " + str2);

        d = Double.parseDouble(str2);
        System.out.println(str2);

        long l = 7436573643098492L;
        String str3 = Long.toString(l);
        System.out.println("str3 = " + str3);

        l = Long.parseLong(str3);
        System.out.println(l);

        char ch = 'y';
        String str4 = Character.toString(ch);
        System.out.println("str4 = " + str4);

        ch = str4.charAt(0);
        System.out.println(ch);

        char ch1 = 'h';
        String str5 = new String(new char[] { ch1 });
        System.out.println("str5 = " + str5);

        ch1 = str5.charAt(0);
        System.out.println(ch1);

        boolean bl_t = true;
        String str6 = Boolean.toString(bl_t);
        System.out.println("str6 = " + bl_t);

        bl_t = Boolean.parseBoolean(str6);
        System.out.println(bl_t);

        boolean bl_f = false;
        String str7 = Boolean.toString(bl_f);
        System.out.println("str7 = " + bl_f);

        bl_f = Boolean.parseBoolean(str7);
        System.out.println(bl_f);




    }
}
