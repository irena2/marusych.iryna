/**
 * Created by Viper on 20.04.2016.
 */
public class Races {
    public static void main(String[] args) {
        Car car1 = new Car1();

        Car car2 = new Car2();

        Car car3 = new Car3();

        for (int i = 0; i < 20; i++) {
            car1.drive();
            car1.turn();
            car2.drive();
            car2.turn();
            car3.drive();
            car3.turn();
        }

        System.out.println("Time for the first car " + Car1.t);
        System.out.println("Time for the second car " + Car2.t);
        System.out.println("Time for the third car " + Car3.t);

        float[] arr = {Car1.t, Car2.t, Car3.t};
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    float tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        System.out.println();
        for (int i=0;i<arr.length;i++) {
            int x=i+1;
            System.out.println("Time for the "+ x +" place = "+arr[i]);
        }
    }
}
