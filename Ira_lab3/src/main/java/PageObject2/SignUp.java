package PageObject2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by SANYA on 12.06.2016.
 */
public class SignUp {
    private WebDriver driver;

    public SignUp (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[@class='text']/span[text()='Facebook' or text()='Google']")
    public WebElement btns;

    public void checkURL() {
        assertTrue("Url doesn't contain 'signup'", driver.getCurrentUrl().contains("signup"));
    }
}
