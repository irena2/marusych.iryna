package PageObject2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.*;

/**
 * Created by SANYA on 12.06.2016.
 */
public class QuestionSection {
    private WebDriver driver;

    public QuestionSection (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//p[contains(@class,'label-key')]/b[text()='today']")
    public WebElement created_today;

    public void checkURL() {
        assertTrue("Url doesn't contain 'questions'", driver.getCurrentUrl().contains("questions"));
    }
}
