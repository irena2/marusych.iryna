package PageObject2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 12.06.2016.
 */
public class MainPage1 {
    private WebDriver driver;

    public MainPage1(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//a[@data-value='featured']/span[text()>300]")
    public WebElement feature_tab;

    @FindBy(xpath = "//a[@class='login-link'][text()='sign up']")
    public WebElement lnk_sign_up;

    public SignUp navigateToSignUp () {
        lnk_sign_up.click();
        return new SignUp(driver);
    }

    @FindBy(xpath = "//a[@class='question-hyperlink'][1]")
    public WebElement question;

    public QuestionSection navigateToQuestionSection () {
        question.click();
        return new QuestionSection(driver);
    }
}
