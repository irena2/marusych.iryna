package PageObject;

import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 12.06.2016.
 */
public class Cities {
    private WebDriver driver;

    public Cities (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Одесса')]")
    public WebElement city_Odessa;
    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Харьков')]")
    public WebElement city_Kharkov;
    @FindBy(xpath = "//div[@class='header-city-i']/*[contains(text(), 'Киев')]")
    public WebElement city_Kiev;


    public void checkURL() {
        assertTrue("Url doesn't contain 'rozetka'", driver.getCurrentUrl().contains("rozetka"));
    }
}
