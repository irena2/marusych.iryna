package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.*;

/**
 * Created by SANYA on 12.06.2016.
 */
public class Basket {
    private WebDriver driver;

    public Basket (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement btn_basket;

    public void checkURL() {
        assertTrue("Url doesn't contain 'rozetka'", driver.getCurrentUrl().contains("rozetka"));
    }
}
