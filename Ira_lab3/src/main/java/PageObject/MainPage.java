package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by SANYA on 12.06.2016.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//img[contains(@alt, 'Rozetka.ua')]")
    public WebElement logo_rozetka;

    @FindBy(xpath = ".//ul[@id='m-main']/li[@menu_id='2195']")
    public WebElement lnk_apple;

    @FindBy(xpath = ".//*[@id='m-main']/li[3]/a[contains(text(), 'MP3')]")
    public WebElement lnk_mp3;

    @FindBy(xpath = ".//a[@name='city']/span")
    public WebElement lnk_city;

    public Cities navigateToCities () {
        lnk_city.click();
        return new Cities(driver);
    }

    @FindBy(xpath = ".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement btn_basket;

    public Basket navigateToBasket () {
        btn_basket.click();
        return new Basket(driver);
    }


}
