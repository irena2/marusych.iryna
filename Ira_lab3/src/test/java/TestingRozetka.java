import PageObject.Basket;
import PageObject.Cities;
import PageObject.MainPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;


/**
 * Created by SANYA on 12.06.2016.
 */
public class TestingRozetka {
    private static WebDriver driver;
    private static MainPage mainPage;
    private static Cities cities;
    private static Basket basket;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//      driver.manage().window().maximize();
        mainPage = new MainPage(driver);
    }
    @Before
    public void startPage () throws Exception {
        driver.get("http://rozetka.com.ua/");
    }
    @AfterClass
    public static void tearDown() throws Exception {
        driver.close();
    }

    @Test
    public void checkLogoIsDisplayed () throws Exception {
        assertTrue("The logo 'Rozetka' can't be found!", mainPage.logo_rozetka.isDisplayed());
    }

    @Test
    public void checkGoodsCatalogContainsApple() throws Exception {
        assertTrue("'Apple' section can't be found within the 'Каталог товаров' menu!", mainPage.lnk_apple.isDisplayed());
    }

    @Test
    public void checkGoodsCatalogContainsMP3() throws Exception {
        assertTrue("'MP3' section can't be found within the 'Каталог товаров'!", mainPage.lnk_mp3.isDisplayed());
    }

    @Test
    public void checkCitySectionContainsLinks() throws Exception {
        cities = mainPage.navigateToCities();
        cities.checkURL();

        assertTrue("'Odessa' link can't be found", cities.city_Odessa.isDisplayed());
        assertTrue("'Kharkiv' link can't be found", cities.city_Kharkov.isDisplayed());
        assertTrue("'Kiev' link can't be found", cities.city_Kiev.isDisplayed());
    }
    @Test
    public void checkBasketIsEmpty() throws Exception {
        basket = mainPage.navigateToBasket();
        basket.checkURL();

        assertTrue("'Basket' isn't empty", basket.btn_basket.isDisplayed());
    }
}
