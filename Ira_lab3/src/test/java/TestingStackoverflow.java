import PageObject2.MainPage1;
import PageObject2.QuestionSection;
import PageObject2.SignUp;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

/**
 * Created by SANYA on 12.06.2016.
 */
public class TestingStackoverflow {
    private static WebDriver driver;
    private static MainPage1 mainPage1;
    private static SignUp signUp;
    private static QuestionSection questionSection;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//      driver.manage().window().maximize();
        mainPage1 = new MainPage1(driver);
    }
    @Before
    public void startPage () throws Exception {
        driver.get("http://stackoverflow.com/");
    }
    @AfterClass
    public static void tearDown() throws Exception {
        driver.close();
    }

    @Test
    public void checkCountOfFeaturedTab() throws Exception {
        assertTrue("Number within the 'featured' tab is less than 300", mainPage1.feature_tab.isDisplayed());
    }

    @Test
    public void checkPresenceOfGoogleFbButtons() throws Exception {
        signUp = mainPage1.navigateToSignUp();
        signUp.checkURL();
        assertTrue("Buttons 'Google' and 'Facebook' are not displayed ", signUp.btns.isDisplayed());
    }

    @Test
    public void checkQuestionIsCreatedToday() throws Exception {
        questionSection = mainPage1.navigateToQuestionSection();
        questionSection.checkURL();
        assertTrue("The question isn't created today", questionSection.created_today.isDisplayed());
    }
}
